#ifndef __BMX280_H__
#define __BMX280_H__

#include <stdint.h>
#include <stdbool.h>

#define BMX280_ADDRESS 0xEC

typedef union { 
    struct {
        uint16_t dig_T1;
        int16_t dig_T2;
        int16_t dig_T3;
    };
    struct {
        uint16_t dig_P1;
        int16_t dig_P2;
        int16_t dig_P3;
        int16_t dig_P4;
        int16_t dig_P5;
        int16_t dig_P6;
        int16_t dig_P7;
        int16_t dig_P8;
        int16_t dig_P9;
    };
    struct {
        uint8_t dig_H1;
        int16_t dig_H2;
        uint8_t dig_H3;
        int16_t dig_H4;
        int16_t dig_H5;
        int8_t dig_H6;
    };
} BMX280CompensationParams;

typedef enum { BMX_UNKNOWN, BMP_280, BME_280 } BMX_MODEL;

void bmx280_reset();

BMX_MODEL bmx280_read_model();

void bmx280_force_measurement(void);

double bmx280_read_compensated_temperature(void);
double bmx280_read_compensated_pressure(void);
double bmx280_read_compensated_humidity(void);

double bmx280_pressure_at_sealevel(double atm_p, int altitude);





#endif /* __BMX280_H__ */
