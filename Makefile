MCU:=atmega8
DUDE_MCU=m8
F_CPU:=11059200UL
DEFS=-D F_CPU=$(F_CPU)
CC=avr-gcc
OBJCOPY=avr-objcopy
DOUBLE64=-mdouble=64
CFLAGS=-O2 -DNDEBUG $(DOUBLE64) -Wno-unused-function\
	   -Wall -pedantic \
	   -mcall-prologues \
	   -mno-interrupts \
	   -fpack-struct \
	   -ffunction-sections \
	   -fdata-sections \
	   -fno-strict-aliasing \
	   -fshort-enums \
	   -flto 

LDOPTS=-flto -fuse-linker-plugin -Wl,--gc-sections
#-ffast-math
#LIBS=-Wl,-u,vprintf -lprintf_flt -lm
LIBS=-lm

.PHONY: printcompiledefs

all: printcompiledefs program.hex data.eep size clean

# START SOURCE COMPILE UNITS SECTION

program.hex: program.elf
	$(OBJCOPY) -j .text -j .data -O ihex $^ $@

program.elf: main.o uart.o twimaster.o bmx280.o rotenc.o display.o
	$(CC) $^ -mmcu=$(MCU) $(LIBS) $(LDOPTS) -o $@
	avr-strip $@

data.eep: program.elf
	@echo extracting eeprom
	$(OBJCOPY) -j .eeprom --set-section-flags=.eeprom="alloc,load" \
	--change-section-lma .eeprom=0 --no-change-warnings -O ihex $< $@ || exit 0
	od -tx1 $@

main.o: main.c 
	$(CC) -mmcu=$(MCU) $(CFLAGS) $(DEFS) $< -c -o $@

uart.o: uart.c uart.h
	$(CC) -mmcu=$(MCU) $(CFLAGS) $(DEFS) $< -c -o $@

twimaster.o: twimaster.c i2cmaster.h
	$(CC) -mmcu=$(MCU) $(CFLAGS) $(DEFS) $< -c -o $@

display.o: display.c display.h
	$(CC) -mmcu=$(MCU) $(CFLAGS) $(DEFS) $< -c -o $@

bmx280.o: bmx280.c bmx280.h
	$(CC) -mmcu=$(MCU) $(CFLAGS) $(DEFS) $< -c -o $@

rotenc.o: rotenc.c rotenc.h
	$(CC) -mmcu=$(MCU) $(CFLAGS) $(DEFS) $< -c -o $@

burnusbasp: program.hex
	avrdude -p $(DUDE_MCU) -c usbasp -P usb -U flash:w:program.hex

burnponyser: program.hex
	avrdude -p $(DUDE_MCU) -c ponyser -P $(TTY) -U flash:w:program.hex \
		-b $(AVRDUDE_BAUDRATE)

burnbuspirate: program.hex
	avrdude -p $(DUDE_MCU) -B 125kHz -c buspirate -P /dev/buspirate -U \
	   	flash:w:program.hex

eepbuspirate: data.epp
	avrdude -p $(DUDE_MCU) -c buspirate -P /dev/buspirate -U eeprom:w:data.eep

size: program.elf
	avr-size -C --mcu=$(MCU) $^

clean:
	rm -f *.o *.bin

printcompiledefs:
	@echo Compilation Parameters
	@echo ----------------------
	@echo F_CPU: $(F_CPU) MHz
	@echo MCU: $(MCU)
	@echo DUDE_MCU: $(DUDE_MCU)
	@echo
	@echo
