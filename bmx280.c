#include "i2cmaster.h"
#include "bmx280.h"
#include <assert.h>
#include <math.h>

#define BMX280_RESET_REGISTER 0xE0
#define BMX280_ID_REGISTER 0xD0

#define BMX280_PRESS_MSB 0xF7
#define BMX280_TEMP_MSB 0xFA

static_assert(sizeof(double) == 8, "assertion of double precision failed, it's better to use fixed point numbers");

static void bmx280_write(const uint8_t register_addr,const uint8_t data)
{
    i2c_start(BMX280_ADDRESS | I2C_WRITE);
    i2c_write(register_addr);
    i2c_write(data);
    i2c_stop();
}

static uint8_t bmx280_read_n(const uint8_t start_register_addr, uint8_t* d, uint8_t n)
{
    i2c_start(BMX280_ADDRESS | I2C_WRITE);
    i2c_write(start_register_addr);
    i2c_rep_start(BMX280_ADDRESS | I2C_READ);
    for (; n > 1; --n) {
        *d++ = i2c_read(true);
    }
    *d = i2c_read(false);
    i2c_stop();
    return d[0];
}

void bmx280_reset()
{
    bmx280_write(BMX280_RESET_REGISTER, 0xB6);
}

BMX_MODEL bmx280_read_model()
{
    uint8_t id = bmx280_read_n(BMX280_ID_REGISTER, &id, 1);
    if (id == 0x56 || id == 0x57 || id == 0x58)
        return BMP_280;
    else
        if (id == 0x60)
            return BME_280;
    return BMX_UNKNOWN;
}

void bmx280_force_measurement()
{
    bmx280_write(0xF2, 0x02); // humidity oversampling x 2
    bmx280_write(0xF3, 0x04); // filter enabled for temperature and pressure
    bmx280_write(0xF4, 0x4A); // force mode
}                            

static inline unsigned long pack_u20(const uint8_t* v)
{
    uint32_t a = v[0]; // msb
    a <<= 8;
    a |= v[1]; // lsb
    a <<= 4;
    a |= ((v[2] >> 4) & 0x0F); // xlsb only one nibble
    return a; 
}

int32_t g_fine_temperature; // aux temperature variable for pressure and humidity compensation

double bmx280_compensate_temperature(int32_t adc_T, const BMX280CompensationParams* cp);
double bmx280_compensate_pressure(int32_t adc_P, const BMX280CompensationParams* cp);
double bmx280_compensate_humidity(int32_t adc_H, const BMX280CompensationParams* cp);

double bmx280_read_compensated_temperature()
{
    BMX280CompensationParams cp;
    uint8_t buf[3]; 
    bmx280_read_n(0x88, buf, 2);
    cp.dig_T1 = (uint16_t)((buf[1] << 8) | buf[0]);
    bmx280_read_n(0x8a, buf, 2);
    cp.dig_T2 = (int16_t)((buf[1] << 8) | buf[0]);
    bmx280_read_n(0x8c, buf, 2);
    cp.dig_T3 = (int16_t)((buf[1] << 8) | buf[0]);
    // Example values, extracted from a real sensor
    //cp.dig_T1 = 0x6f9c;
    //cp.dig_T2 = 0x6854;
    //cp.dig_T3 = 0x0032;
    
    bmx280_read_n(BMX280_TEMP_MSB, buf, 3); // MSB LSB XLSB
    int32_t temperature_read = pack_u20(buf);
    return bmx280_compensate_temperature(temperature_read, &cp);
}

double bmx280_read_compensated_pressure()
{
    BMX280CompensationParams cp;
    const int PRESSURE_CP_SIZE = 9*2;
    uint8_t buf[PRESSURE_CP_SIZE]; 
    bmx280_read_n(0x8E, buf, PRESSURE_CP_SIZE);
    cp.dig_P1 = (uint16_t)(buf[0] | (buf[1] << 8));
    cp.dig_P2 = (int16_t)(buf[2] | (buf[3] << 8));
    cp.dig_P3 = (int16_t)(buf[4] | (buf[5] << 8));
    cp.dig_P4 = (int16_t)(buf[6] | (buf[7] << 8));
    cp.dig_P5 = (int16_t)(buf[8] | (buf[9] << 8));
    cp.dig_P6 = (int16_t)(buf[10] | (buf[11] << 8));
    cp.dig_P7 = (int16_t)(buf[12] | (buf[13] << 8));
    cp.dig_P8 = (int16_t)(buf[14] | (buf[15] << 8));
    cp.dig_P9 = (int16_t)(buf[16] | (buf[17] << 8));

    bmx280_read_n(BMX280_PRESS_MSB, buf, 3); // MSB LSB XLSB
    int32_t pressure_read = pack_u20(buf);
    return bmx280_compensate_pressure(pressure_read, &cp);
}

double bmx280_read_compensated_humidity()
{
   BMX280CompensationParams cp;
   uint8_t buf[3];
   cp.dig_H1 = bmx280_read_n(0xA1, buf, 1);
   
   bmx280_read_n(0xE1, buf, 3);
   cp.dig_H2 = (int16_t)((buf[1] << 8) | buf[0]);
   cp.dig_H3 = buf[2];

   bmx280_read_n(0xE4, buf, 3);
   cp.dig_H4 = (int16_t)((buf[0] << 4) | (buf[1] & 0x0F));
   cp.dig_H5 = (buf[1] >> 4) | ((int16_t)buf[2] << 4);
   cp.dig_H6 = bmx280_read_n(0xE7, buf, 1);


   bmx280_read_n(0xFD, buf, 2);
   int32_t humidity_read = ((int16_t)buf[0] << 8) | buf[1];
   return bmx280_compensate_humidity(humidity_read, &cp);
}

double bmx280_pressure_at_sealevel(double atm_p, int altitude)
{
/* equation for computing the pressure is valid for altitude
 * up to 10km:
 *
 *  p0 = p / pow((1 + altitude * (L/T0)) , -(gn*M)/(R*L) );
 *
 *  p - pressure measured [ hPa ]
 *  L - temperature lapse -0.0065 [ K / m ]
 *  T0 - standard temperature at sea level 288.15 K (15 °C)
 *  gn - standard gravity 9.80665 [ m / s*s ]
 *  M - average molar mass of dry air 0.0289644 [ kg / mol ]
 *  R - gas constant 8.314462611815324 [ (N * m) / (mol*K) ]
*/

const double ALT_T_COEFF = -0.00002255769564462953; // L/T0
const double EXPONENT = 5.25578596313068172618; // -(gn*M)/(R*L) 
   
    return atm_p/pow(1 + altitude*ALT_T_COEFF, EXPONENT);
}


double bmx280_compensate_temperature(int32_t adc_T, const BMX280CompensationParams* cp)
{
    double var1, var2, T;
    var1 = (((double)adc_T)/16384.0 - ((double)cp->dig_T1)/1024.0) * ((double)cp->dig_T2);
    var2 = ((((double)adc_T)/131072.0 - ((double)cp->dig_T1)/8192.0) *
            (((double)adc_T)/131072.0 - ((double)cp->dig_T1)/8192.0)) * ((double)cp->dig_T3);
    g_fine_temperature = (int32_t)(var1 + var2);
    T = (var1 + var2) / 5120.0;
    return T;
}


double bmx280_compensate_pressure(int32_t adc_P, const BMX280CompensationParams* cp)
{
    double var1, var2, p;
    var1 = ((double)g_fine_temperature/2.0) - 64000.0;
    var2 = var1 * var1 * ((double)cp->dig_P6) / 32768.0;
    var2 = var2 + var1 * ((double)cp->dig_P5) * 2.0;
    var2 = (var2/4.0)+(((double)cp->dig_P4) * 65536.0);
    var1 = (((double)cp->dig_P3) * var1 * var1 / 524288.0 + ((double)cp->dig_P2) * var1) / 524288.0;
    var1 = (1.0 + var1 / 32768.0)*((double)cp->dig_P1);
    if (var1 == 0.0)
    {
        return 0; // avoid exception caused by division by zero
    }
    p = 1048576.0 - (double)adc_P;
    p = (p - (var2 / 4096.0)) * 6250.0 / var1;
    var1 = ((double)cp->dig_P9) * p * p / 2147483648.0;
    var2 = p * ((double)cp->dig_P8) / 32768.0;
    p = p + (var1 + var2 + ((double)cp->dig_P7)) / 16.0;
    return p;
}

double bmx280_compensate_humidity(int32_t adc_H, const BMX280CompensationParams* cp)
{
    double var_H;
    var_H = (((double)g_fine_temperature) - 76800.0);
    var_H = (adc_H - (((double)cp->dig_H4) * 64.0 + ((double)cp->dig_H5) / 16384.0 *
                var_H)) * (((double)cp->dig_H2) / 65536.0 * (1.0 + ((double)cp->dig_H6) /
                    67108864.0 * var_H *
                    (1.0 + ((double)cp->dig_H3) / 67108864.0 * var_H)));
    var_H = var_H * (1.0 - ((double)cp->dig_H1) * var_H / 524288.0);
    if (var_H > 100.0)
        var_H = 100.0;
    else if (var_H < 0.0)
        var_H = 0.0;
    return var_H;
}



