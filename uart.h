#ifndef __UART_H__
#define __UART_H__

/*
 * Define UART_SET_STDOUT before this file
 * to redirect standard puts, printf,... to UART
 * Remember to assign stdout
 *     stdout = &uart_stdout;
 *
*/

#define HexLO( x ) (( ((x)&0x0F) < 10 ) ? \
        ( ((x)&0x0F) + '0') : ( ((x)&0x0F) + 'a' - 10 ))
#define HexHI( x ) (( ((unsigned char)(x) >> 4) < 10) ? \
      ( ((unsigned char)(x) >> 4) + '0') : ((unsigned char)(x) >> 4) + 'a' - 10)

#define UART_DATA_PRESENT 1

#define BAUD 115200

#define UART_NEWLINE "\r\n"

#ifdef __cplusplus
extern "C" {
#endif

// Initialize UART with baudrate equal to BAUD.
void uart_init();

void uart_putchar(char);

char uart_stat(void);

char uart_getchar(void);

void uart_disable(void); // used when uart pins have other function

void uart_enable(void); // used for reenabling uart after disabled

#define uart_puthex8(c) do \
{ uart_putchar(HexHI(c)); uart_putchar(HexLO(c)); } \
while(0)

void uart_puts(const char*);

void uart_puts_P(const char*);

#if defined(UART_SET_STDOUT)
    #include <stdio.h>
    int uart_putc(char, FILE*);
    int uart_putc_CRNL(char, FILE*); // inserts \r\n for \n
    int uart_fputs(char*, FILE*);

    static FILE uart_stdout =
        FDEV_SETUP_STREAM(uart_putc_CRNL, NULL, _FDEV_SETUP_WRITE);
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __UART_H__ */
