#ifndef __ROTENC_H__
#define __ROTENC_H__

#define ROTENC_DIR_PORT DDRD
#define ROTENC_PORT PORTD 
#define ROTENC_INPUT_PORT PIND
#define ROTENC_CHA_INT INT1
#define ROTENC_CHB_PIN 4 

/* Uncomment this if the circuit inverts logic levels */
// #define ROTENC_INPUTS_INVERTED


/* This library provides simple incrementing and decrementing
 * of value rotenc_value on rotating of the encoder knob.
 *
 * When used with debouncing circuit that inverts inputs
 * uncomment ROTENC_INPUTS_INVERTED definition!
 *
 * WARNING!
 * Hardware debouncing is necessary for reliable work.
 *
 * Usually the debouncing circuit consists of smoothing
 * capacitor and a buffer/inverter with Schmitt triggered
 * inputs. Commonly used 74HC14 or CD40106 chips.
 *
 *            VCC   
 *            |
 *            <
 *            > 10k
 *            <                \
 *            |        10k    | \
 *   +--------+------^V^V^V---|  >o---- to MCU 
 *   |        |               | /
 *   |        |               /
 *   O       --- 0.1µF    
 *    /      ---
 *   *        |
 *   |        |
 *   |        |
 *   GND      GND
*/

typedef unsigned int rotenc_t;

void rotenc_init(void);

void rotenc_interrupt_fn(void); // changes rotenc_value wich should be externally linked

void rotenc_set(rotenc_t);

rotenc_t rotenc_accumulator(void);

void rotenc_clear(void);


#endif /* __ROTENC_H__ */
