# Baro-Hygro-Thermometer #

## Purpouse of the device

The device measures temperature, air pressure and humidity and shows it 
on the display. Unfortunatelly the chips for precise measurement have
very complex refinement procedures after the raw sensor reading is
completed. Therefore the calculations cannot fit into 2K of program 
memory, a 4K chip has to be used.

## Pinouts

```
                 Atmega8 DIP28                     KANDA 6-pin    
                                          
                    ,----v----.                       MISO [* *] VCC
           [/RST] -| 1        28|- PC5 <SCL>           SCK  [* *] MOSI
              PD0 -| 2        27|- PC4 <SDA>           RST  [* *] GND
        <TXD> PD1 -| 3        26|- PC3 <disp:D3>
 <rotenc:BTN> PD2 -| 4(INT0)  25|- PC2 <disp:D2>
 <rotenc:CHA> PD3 -| 5(INT1)  24|- PC1 <disp:D1>
 <rotenc:CHB> PD4 -| 6        23|- PC0 <disp:D0>
              Vcc -| 7        22|- GND
              GND -| 8        21|- AREF 
              X1  -| 9(X1)    20|- AVCC
              X2  -|10(X2)    19|- PB5 [SCK]
              PD5 -|11        18|- PB4 <disp:D6> [MISO]
    <disp:A1> PD6 -|12        17|- PB3 <dips:D5> [MOSI]
    <disp:A1> PD7 -|13        16|- PB2 <disp:D4> 
    <disp:WR> PB0 -|14        15|- PB1 
                   `-----------'   
```

# Peripherial connections

1. Programming ISP -- marked with [ ] braces

   RST, SCK (PB7), MISO (PB8), MOSI (PB5)

2. Uart
    
   TX (PD1) 
   
3. Rotary encoder input

   - Standard pinout for encoder equipped with pullup resistors:

```
        _______________
         |  |  |  |  |
         G  V  S  B  A
         N  C  W
         D  C 
```

   INT0 - button
   INT1 - rotary encoder channel A
   PD4  - rotary encoder channel B


4. Display.

   Display needs 7 lines of data, one line of WR line, and two address lines.

   - Address: PD0, PD1
   - Data PB0 - PB6
   - WR PB7

   Pinout (TOP-VIEW):

                        +----U----+
                   \WR -| 1     14|- GND
                    A1 -| 2     13|- \CLR
                    A2 -| 3     12|- \BL
                   Vcc -| 4     11|- D6
                    D0 -| 5     10|- D5
                    D1 -| 6      9|- D4
                    D2 -| 7      8|- D3
                        +---------+



## Data readout from BME280

Data readout is done by starting a burst read from 0xF7 to 0xFC
 (temperature and pressure)
or from 0xF7 to 0xFE (temperature, pressure and humidity). 
The data are read out in an
unsigned 20-bit format both for pressure and for temperature 
and in an unsigned 16-bit format for humidity.



## Led driver chips



### How do I get set up? ###

### Contribution guidelines ###

