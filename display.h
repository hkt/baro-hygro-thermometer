#ifndef __DISPLAY_H__
#define __DISPLAY_H__

void display_init(void);
void display_cstr(const char*);

#endif /* __DISPLAY_H__ */
