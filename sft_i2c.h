#ifndef __SFT_I2C_H__
#define __SFT_I2C_H__

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

/*
 * API based on 
 * Bernhard Nebel and Peter Fleury's
 * SoftI2CMaster
*/

#define I2C_SPEED_100KHZ
#define I2C_PORT_LETTER D
#define I2C_SDA_PIN 6
#define I2C_SCL_PIN 5 

#ifdef __cplusplus
extern "C" {
#endif

/* Init
*  Returns false if any of the lines is low (blocked or broken device)
*/
bool i2c_init(void);

#define I2C_READ 1
#define I2C_WRITE 0

/* Start of operation
*  Returns true if ACK is received
*/
bool i2c_start(uint8_t addr);

/* Start again without stop
*  Returns true if ACK is received
*/
bool i2c_rep_start(uint8_t addr);

/* Stop condition - free the bus
*/ 
void i2c_stop(void);

bool i2c_write(uint8_t val);

bool i2c_write_n(const uint8_t* s, size_t n);

/* Read byte
 * If ack == true -> ACK is sent. 
 * If ack == false -> NACK is sent (usually end of multibyte read).
*/
uint8_t i2c_read(bool ack);

#ifdef __cplusplus
}
#endif


#endif /* __SFT_I2C_H__ */
