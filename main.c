#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
//#include <avr/pgmspace.h>
#include <stdbool.h>
#include "uart.h"
#include "bmx280.h"
#include "i2cmaster.h"
#include "display.h"
#include <avr/interrupt.h>
#include <string.h>
#include <avr/eeprom.h>


#define ROTENC_INPUTS_INVERTED 1
#include "rotenc.h"

// arbitrary chosen address
#define ALTITUDE_EEP_ADDR ((uint16_t*)23)


uint16_t EEMEM ee_altitude_m = 200;
volatile uint16_t g_altitude;

typedef struct {
   double temperature;
   double pressure;
   double humidity;
   bool is_reading_valid;
} Air;


typedef struct {
    unsigned char button_pressed: 1;
    unsigned char label_on: 1;
} Flags;


void timer_sec_init(void)
{
    cli();
    /* Timer clock = F_IO_clk/1024 */
    TCCR0 = (1 << CS02)|(1<<CS00);
    /* Clear overflow flag */
    TIFR |= 1 << TOV0;
    /* Enable overflow interrupt */
    TIMSK |= 1 << TOIE0;
    TCNT0 = 0;
    sei();
}

void button_irq_init(void)
{
    cli();
    MCUCR |= (1<<ISC01); // falling edge of INT0
    GICR |= (1<<INT0);
    GIFR |= (1<<INTF0);
    sei();
}


typedef unsigned char short_time_t; // must increment atomically

short_time_t g_time;

ISR (TIMER0_OVF_vect)
{
    static uint8_t ovf_tick;
    const unsigned char TICKS_PER_SECOND = F_CPU/1024/256;
    if (ovf_tick >= TICKS_PER_SECOND) {
        ovf_tick = 0;
        ++g_time;
    }
    else
        ++ovf_tick;
}

void store_altitude(uint16_t altitude)
{
    eeprom_write_word(ALTITUDE_EEP_ADDR, ee_altitude_m);
}


ISR(INT1_vect)
{
    rotenc_interrupt_fn();
}

Flags g_flags = { 0, 0 };

ISR(INT0_vect)
{
    g_flags.button_pressed = true;
}

Air measure()
{
    /* Check if the sensor is present */
    BMX_MODEL model = bmx280_read_model();
    if (model == BME_280) {
        bmx280_force_measurement();
        _delay_ms(500);
        Air air;
        air.temperature = bmx280_read_compensated_temperature();
        air.pressure = bmx280_read_compensated_pressure();
        air.humidity = bmx280_read_compensated_humidity();
        air.is_reading_valid = true;
        return air;
    }
    else {
        Air invalid_result = { 0, 0, false }; 
        return invalid_result;
    }
}

/* Display value left padded */
void display_flt(double value)
{
    char buf[5] = {0};
    uart_puts(dtostrf(value, sizeof(buf) - 1, 0, buf));
    display_cstr(buf);
}


typedef enum { TEMPERATURE, PRESSURE, HUMIDITY, ALTITUDE_SET } MODE;
const char* MODE_NAMES[] = { "st.C", " hPa", "%wzg", "mnpm" };

MODE next_mode(MODE current_mode)
{
    switch (current_mode)
    {
        case TEMPERATURE:
        case ALTITUDE_SET:
            return PRESSURE;
        case PRESSURE:
            return HUMIDITY;
        case HUMIDITY:
        default:
            return TEMPERATURE;
    }
}


bool display_property_if_valid(const Air* air, MODE mode)
{
    if (air->is_reading_valid == false)
        return false;
    switch (mode) {
        case TEMPERATURE:
        display_flt(air->temperature);
        return true;
        case PRESSURE:
        display_flt( // value is divided by 100 Pa -> hPa 
            bmx280_pressure_at_sealevel(air->pressure/100, g_altitude));
        return true;
        break;
        case HUMIDITY:
        display_flt(air->humidity);
        return true;
        default:
            return false;
    }
    return false;
}



unsigned char seconds_elapsed(short_time_t start, short_time_t end)
{
    // Detect counter overflow and adjust
    return (end >= start) ?
            end - start :
            ((short_time_t)~0 - start) + end + 1;
}

enum PRESS_TYPE { SHORT_PRESS, LONG_PRESS = 2, OVERLONG_PRESS = 6 };

enum PRESS_TYPE press_type(short_time_t duration) {
    if (duration < LONG_PRESS)
        return SHORT_PRESS;

    if (LONG_PRESS <= duration && duration < OVERLONG_PRESS)
        return LONG_PRESS;

    return OVERLONG_PRESS;
}

/* Operations:
 * 
 *  g_flags | pvev_flags |                     action
 * --------------------------------------------------------------------------
 *     -    |   pressed  | => on release -> {SHORT_,LONG_}PRESS -> mode change
 *  pressed |   pressed  | => held -> LONG_PRESS ? -> mode change
 *  pressed |      -     | => save time of beginning
 *     -    |      -     | => nothing
*/


int main(void)
{
    uart_init();
    uart_puts("BHT (" __DATE__ ")"); 
    i2c_init();
    bmx280_reset();
    timer_sec_init();
    button_irq_init();
    display_init();
    g_altitude = eeprom_read_word(ALTITUDE_EEP_ADDR);
    rotenc_init();
    rotenc_clear();

    uart_puts("Device initialized");
    uart_puts("The size of double is: ");
    int double_size = sizeof(double);
    char buf[2] = { '0' + double_size, 0 };
    uart_puts(buf);
    display_cstr("::::");
    MODE mode = TEMPERATURE;
    _delay_ms(250);
    
    Flags prev_flags = g_flags;

    short_time_t LABEL_TIMEOUT = 1;
    short_time_t press_time, chmode_time;

    for (;;) {
        // input section
        if (g_flags.button_pressed) {
            if (prev_flags.button_pressed) {
                short_time_t duration = seconds_elapsed(press_time, g_time);
                if (press_type(duration) == LONG_PRESS) {
                    mode = ALTITUDE_SET;
                    chmode_time = g_time;
                    g_flags.label_on = true;
                }
            }
            else  // just has been pressed
                press_time = g_time;
        }
        else {
            if (prev_flags.button_pressed) {
                short_time_t duration = seconds_elapsed(press_time, g_time);
                switch (press_type(duration)) {
                    case SHORT_PRESS:
                       mode = next_mode(mode);
                       chmode_time = g_time;
                       g_flags.label_on = true;
                    break;
                    case LONG_PRESS:
                       mode = ALTITUDE_SET;
                       chmode_time = g_time;
                       g_flags.label_on = true;
                    break;
                    default:
                    case OVERLONG_PRESS:
                    break;
                }
            }
        }
        // display section
        if (g_flags.label_on && seconds_elapsed(chmode_time, g_time) < LABEL_TIMEOUT) {
            display_cstr(MODE_NAMES[mode]);
            rotenc_clear();
        }
        else {
            g_flags.label_on = false;
            if (mode == ALTITUDE_SET) {
                g_altitude += rotenc_accumulator();
                rotenc_clear();
                if (g_time % 1)
                    display_flt(g_altitude);
                else
                    display_cstr("    ");
            }
            else {
                Air air = measure();
                bool success = display_property_if_valid(&air, mode);
                if (!success) {
                    uart_puts("Invalid data");
                    display_cstr("Blad");
                }
            }
        }
        prev_flags = g_flags;
        _delay_ms(20);
    }
}
