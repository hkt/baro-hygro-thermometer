#include <stdbool.h>
#include "sft_i2c.h"
#include <util/delay.h>
#include <avr/io.h>
#include <limits.h>
#include <assert.h>


#ifndef CONCAT2
#define _CCAT2(a,b) a##b
#define CONCAT2(a,b) _CCAT2(a,b)
#endif

#define I2C_PORT CONCAT2(PORT,I2C_PORT_LETTER)
#define I2C_DIRP CONCAT2(DDR,I2C_PORT_LETTER)
#define I2C_INPORT CONCAT2(PIN,I2C_PORT_LETTER)

#ifdef I2C_SPEED_100KHZ
    #define I2C_SPEED 100000UL
#else
    #define I2C_SPEED 400000UL
#endif

    
/*              DELAY CALCULATION
 *
 * Two most popular frequencies of I2C bus are:
 *  100kHz - period lasts 10 us
 *   => SCL state (high and low) should be held for 4.7 us
 *  400kHz - period lasts 2.5 us
 *   => SCL state (high and low) should be held for 1.3 us
 *
 *  Given the F_CPU we can count how many cycles to spend
 *  for HALF_PERIOD and for QUARTER_PERIOD
 *
 *  SCL  -----------            -------     
 *      |           |          |
 *  ====/-----------\==========/-----------------------
 *       HALF_PERIOD               ^
 *                              Probe after QUARTER_PERIOD
*/

#define I2C_HPERIOD_CYCLES (F_CPU/I2C_SPEED/2)
#define I2C_QPERIOD_CYCLES (F_CPU/I2C_SPEED/4)

#if (I2C_HPERIOD_CYCLES/3) < 255
/* _delay_loop1 executes 4*arg cycles; 0 gives 256 cycles so check is provided */
#define i2c_half_period_delay() _delay_loop_1((I2C_HPERIOD_CYCLES/3 == 0) ? 1 : I2C_HPERIOD_CYCLES/3)
#define i2c_quarter_period_delay() _delay_loop_1((I2C_QPERIOD_CYCLES/3 == 0) ? 1 : I2C_QPERIOD_CYCLES/3)
#else
/* _delay_loop2 executes 4*arg cycles; 0 gives 65536 cycles so check is provided */
#define i2c_half_period_delay() _delay_loop_2((I2C_HPERIOD_CYCLES/4 == 0) ? 1 : I2C_HPERIOD_CYCLES/4)
#define i2c_quarter_period_delay() _delay_loop_2((I2C_QPERIOD_CYCLES/4 == 0) ? 1 : I2C_QPERIOD_CYCLES/4)
#endif /* (I2C_HPERIOD_CYCLES) overflow check */

#define SCL_TIMEOUT 200 

#define i2c_SDA_low() do { I2C_DIRP |= (1<<I2C_SDA_PIN); } while (0)
#define i2c_SCL_low() do { I2C_DIRP |= (1<<I2C_SCL_PIN); } while (0)
#define i2c_let_SDA_high() do { I2C_DIRP &= ~(1<<I2C_SDA_PIN); } while (0) 
#define i2c_let_SCL_high() do { I2C_DIRP &= ~(1<<I2C_SCL_PIN); } while (0) 
#define i2c_is_SDA_high() (I2C_INPORT & (1<<I2C_SDA_PIN))
#define i2c_is_SCL_high() (I2C_INPORT & (1<<I2C_SCL_PIN))


inline static void i2c_let_SDA_high_if(bool cond)
{
    if (cond)
        i2c_let_SDA_high();
    else
        i2c_SDA_low();
}

static bool i2c_wait_SCL_high(void)
{
    unsigned char counter = SCL_TIMEOUT;
    _Static_assert(SCL_TIMEOUT <= UCHAR_MAX, "SCL_TIMEOUT too high");
    do {
        if (i2c_is_SCL_high())
            return true;
        i2c_quarter_period_delay();
    } while (--counter);
    return false;
}

bool i2c_init(void)
{
    I2C_PORT &= ~(1 << I2C_SDA_PIN);
    I2C_PORT &= ~(1 << I2C_SCL_PIN);
    i2c_let_SDA_high();
    i2c_let_SCL_high();
    /* Assure pull-ups work and the bus isn't blocked. */
    return (i2c_is_SDA_high() && i2c_is_SCL_high());
}

bool i2c_start(uint8_t addr)
{
    /* Assume we are after init SCL==high SDA==high */
    i2c_half_period_delay();
    i2c_SDA_low(); // while SCL is still high
    i2c_half_period_delay();
    i2c_half_period_delay();
    i2c_SCL_low();
    return i2c_write(addr);
}

void i2c_stop(void)
{
    /* Assume both SCL, SDA are low. */
    i2c_half_period_delay();
    i2c_let_SCL_high();
    i2c_half_period_delay();
    i2c_half_period_delay();
    i2c_let_SDA_high(); // while SCL is high
    // here usually devices demand longer delay for internal operations
}

bool i2c_rep_start(uint8_t addr)
{
    i2c_half_period_delay();
    i2c_let_SDA_high();
    i2c_half_period_delay();
    i2c_let_SCL_high();
    i2c_wait_SCL_high(); // allow clock stretching
    return i2c_start(addr);
}

bool i2c_check_ack(void)
{
    i2c_let_SDA_high();
    i2c_half_period_delay();
    i2c_let_SCL_high(); 
    i2c_wait_SCL_high();
    i2c_half_period_delay();
    bool ack = !i2c_is_SDA_high(); 
    i2c_quarter_period_delay();
    i2c_SCL_low();
    i2c_SDA_low();
    return ack;
}

bool i2c_write(uint8_t byte)
{
    uint8_t mask;
    for (mask = 0x80; mask; mask>>=1) {
        i2c_let_SDA_high_if(byte & mask);
        i2c_half_period_delay();
        i2c_let_SCL_high(); // clock out the bit
        i2c_wait_SCL_high(); // allow clock stretching
        i2c_half_period_delay();
        i2c_SCL_low();
    }
    return i2c_check_ack();
} 

bool i2c_write_n(const uint8_t* s, size_t n)
{
    const uint8_t* end = s + n;
    bool ack = (n > 0); // if n == 0 then no loop
    while (s != end) {
        ack = (i2c_write(*s++)) ? ack : false;
    }
    return ack;
}

void i2c_read_n(uint8_t* d, size_t n)
{
    if (n == 0)
        return;
    const uint8_t* end = d + n;
    while (d != end - 1) {
        *d++ = i2c_read(true);
    }
    *d = i2c_read(false); // end of multibyte read
}

uint8_t i2c_read(bool ack)
{
    uint8_t mask = 0x80, resp = 0;
    i2c_let_SDA_high();
    while (mask) {
        i2c_SCL_low();
        i2c_half_period_delay();
        i2c_let_SCL_high();
        i2c_quarter_period_delay();

        if (i2c_is_SDA_high())
            resp |= mask;
        mask >>= 1;

        i2c_quarter_period_delay();
    }
    i2c_SCL_low();
    if (!ack)
        i2c_let_SDA_high();
    else
        i2c_SDA_low();
    i2c_half_period_delay();
    i2c_let_SCL_high();
    i2c_wait_SCL_high();
    i2c_half_period_delay();
    i2c_SCL_low();
    i2c_quarter_period_delay();
    i2c_SDA_low();
    return resp;
}

