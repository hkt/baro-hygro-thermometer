#include <stdio.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/io.h>
#include "uart.h"
#include <util/setbaud.h>


void uart_init(void) {
    // Set the proper baudrate
    #if defined(UBRRH) && defined(UBRRL)
        UBRRH = UBRRH_VALUE;
        UBRRL = UBRRL_VALUE;
    #elif defined(UBRR)
        UBRR = UBRR_VALUE;
    #endif

    #if defined(UCSRA)
        #if USE_2X
            UCSRA |= (1<<U2X);
        #else
            UCSRA &= ~(1<<U2X);
        #endif /* USE_2X */
    #endif /* UCSRA not present in older MCUs */

    // Transmission parameters
    #if defined(UCSRC)
    #if defined(URSEL)
        UCSRC = (1 << URSEL) | (3 << UCSZ0);	// async 8n1
    #else
        UCSRC = (3 <<  UCSZ0);
    #endif /* URSEL */
    #elif defined(UCR)
        UCR = (1 << RXEN) | (1 << TXEN);
    #endif /* UCSRC */

    uart_enable();
}


void uart_putchar(char c) {
    #if defined(USR)
        loop_until_bit_is_set(USR, UDRE);
    #elif defined(UCSRA)
        loop_until_bit_is_set(UCSRA, UDRE);
    #endif
    UDR = c;
}

char uart_stat(void) {
    #if defined(USR)
        if (USR & (1<<RXC))
    #elif defined(UCSRA)
            if (UCSRA & (1<<RXC))
    #endif
                return UART_DATA_PRESENT;
        return 0;
}

char uart_getchar(void) {
    while (uart_stat() != UART_DATA_PRESENT)
        ;
    return UDR;
}

void uart_disable(void) {
    #if defined(UCR)
        UCR &= ~((1 << RXEN) | (1 << TXEN));
    #elif defined(UCSRB)
        UCSRB &= ~((1 << RXEN) | (1 << TXEN));
    #endif
}

void uart_enable(void) {
    // Enable TX and RX
    #if defined(UCSRB)
        UCSRB = (1 << RXEN) | (1 << TXEN);
    #elif defined(UCR)
        UCR = (1 << RXEN) | (1 << TXEN);
    #endif /* UCR || USCRB */
}

int uart_putc(char c, FILE* stream) {
    uart_putchar(c);
    return 0;
}

int uart_putc_CRNL(char c, FILE* stream) {
    if (c == '\n')
        uart_putchar('\r');
    uart_putchar(c);
    return 0;
}


int uart_fputs(const char* s, FILE* stream) {
    while (*s) {
        uart_putchar(*s);
        s++;
    }
    return 0;
}

void uart_puts(const char* s) {
    while (*s) {
        uart_putchar(*s);
        s++;
    }
    const char* nl = UART_NEWLINE;
    while (*nl) {
        uart_putchar(*nl);
        nl++;
    }
}

void uart_puts_P(const char* s) {
    char c;
    while( 0 != (c = pgm_read_byte(s++)) )
        uart_putchar(c);
    const char* nl = UART_NEWLINE;
    while (*nl) {
        uart_putchar(*nl);
        nl++;
    }
}
