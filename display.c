#include <avr/io.h>
#include <util/delay.h>
#include "display.h"

/* Simple ascii display with WR pin activation when pulled low */

#define DISP_LO_NIBBLE_DIR_PORT DDRC
#define DISP_LO_NIBBLE_PORT PORTC
#define DISP_LO_NIBBLE_OFFSET 0

#define DISP_HI_NIBBLE_DIR_PORT DDRB
#define DISP_HI_NIBBLE_PORT PORTB
#define DISP_HI_NIBBLE_OFFSET 2 // from PBn to PB(n+3)

#define DISP_ADDR_DIR_PORT DDRD
#define DISP_ADDR_PORT PORTD
#define DISP_A0_MASK (1<<6)
#define DISP_A1_MASK (1<<7)
#define DISP_LEN 4
#define DISP_WR_PIN 0 


void display_init(void)
{
    // Low nibble port bits as output
    DISP_LO_NIBBLE_DIR_PORT |= (0x0F) << DISP_LO_NIBBLE_OFFSET;
    // Hi nibble port bits as output together with WR pin
    DISP_HI_NIBBLE_DIR_PORT |= (0x07) << DISP_HI_NIBBLE_OFFSET;
    DISP_HI_NIBBLE_DIR_PORT |= 1 << DISP_WR_PIN;
    // Set /WR pin default to hi
    DISP_HI_NIBBLE_PORT |= 1 << DISP_WR_PIN;
    // Address pins as outputs
    DISP_ADDR_DIR_PORT |= DISP_A0_MASK|DISP_A1_MASK;
}



static void select_address(unsigned char pos)
{
    DISP_ADDR_PORT &= ~(DISP_A0_MASK|DISP_A1_MASK);
    switch (pos) {
        case 0:
            DISP_ADDR_PORT &= ~(DISP_A0_MASK);
            DISP_ADDR_PORT &= ~(DISP_A1_MASK);
        break;
        case 1:
            DISP_ADDR_PORT |= DISP_A0_MASK;
            DISP_ADDR_PORT &= ~(DISP_A1_MASK);
        break;
        case 2:
            DISP_ADDR_PORT &= ~(DISP_A0_MASK);
            DISP_ADDR_PORT |= DISP_A1_MASK;
        break;
        case 3:
            DISP_ADDR_PORT |= DISP_A0_MASK;
            DISP_ADDR_PORT |= DISP_A1_MASK;
        break;
    }
}
 
static void disp_putchar(char c)
{
    DISP_LO_NIBBLE_PORT &= ~(0x0F << DISP_LO_NIBBLE_OFFSET);
    DISP_LO_NIBBLE_PORT |= (c & 0x0F) << DISP_LO_NIBBLE_OFFSET;  
    DISP_HI_NIBBLE_PORT &= ~(0x07 << DISP_HI_NIBBLE_OFFSET);
    DISP_HI_NIBBLE_PORT |= ((c & 0x70) >> 4) << DISP_HI_NIBBLE_OFFSET; 
    // write slope
    DISP_HI_NIBBLE_PORT &= ~(1 << DISP_WR_PIN);
    _delay_us(3);
    DISP_HI_NIBBLE_PORT |= 1 << DISP_WR_PIN;
    _delay_us(3);
}


void display_cstr(const char* buffer)
{
    _delay_us(3);
    unsigned char i;
    for (i = 0; i < DISP_LEN; ++i) {
        if (buffer[i] == '\0')
            break; // don't care if not refreshed all
        select_address(DISP_LEN - i -1);
        disp_putchar(buffer[i]);
    }
}
