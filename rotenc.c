#include "rotenc.h"
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
static volatile rotenc_t rotenc_value;

#if ROTENC_CHA_INT == INT0
  #define INTERRUPT_MASK ((1 << ISC01) | (1 << ISC00)) 
  #define INTERRUPT_FALLING_EDGE ((1 << ISC01) | (0 << ISC00))
  #define INTERRUPT_RISING_EDGE ((1 << ISC01) | (1 << ISC00)) 
#elif ROTENC_CHA_INT == INT1
  #define INTERRUPT_MASK ((1 << ISC11) | (1 << ISC10))
  #define INTERRUPT_FALLING_EDGE ((1 << ISC11) | (0 << ISC10))
  #define INTERRUPT_RISING_EDGE ((1 << ISC11) | (1 << ISC10)) 
#else
  #error ROTENC_CHA_INT must be INT0 or INT1
#endif

typedef enum { RISING_EDGE, FALLING_EDGE } INT_MODE;

void rotenc_setup_irq(INT_MODE int_mode)
{
    if (int_mode == RISING_EDGE) 
        MCUCR = (MCUCR & ~INTERRUPT_MASK) | INTERRUPT_RISING_EDGE;
    else // int_type == ROTENC_CHA_FALLING
        MCUCR = (MCUCR & ~INTERRUPT_MASK) | INTERRUPT_FALLING_EDGE;

    GIMSK |= (1 << ROTENC_CHA_INT);  // enable interrupt 
}

void rotenc_init(void)
{
    cli();
    rotenc_setup_irq(FALLING_EDGE);
    ROTENC_DIR_PORT &= ~(1 << ROTENC_CHB_PIN); // CHB PIN as input
    ROTENC_PORT |= (1 << ROTENC_CHB_PIN); // pull-up on
    sei();
}

static inline INT_MODE read_chA_int_mode()
{
    if ((MCUCR & INTERRUPT_MASK) == INTERRUPT_RISING_EDGE)
        return RISING_EDGE;
    if ((MCUCR & INTERRUPT_MASK) == INTERRUPT_FALLING_EDGE)
        return FALLING_EDGE;
    abort(); 
    return 3;
}


static inline uint8_t read_chB()
{
    return (ROTENC_INPUT_PORT & (1 << ROTENC_CHB_PIN));
}

void rotenc_interrupt_fn()
{
    switch(read_chA_int_mode()) {
        case RISING_EDGE:
#if defined(ROTENC_INPUTS_INVERTED)
        rotenc_value -= ((read_chB() == 0) ? +1 : -1); 
#else
        rotenc_value += ((read_chB() == 0) ? +1 : -1);  // if B is low then CW else CCW 
#endif /* ROTENC_INPUTS_INVERTED */
        rotenc_setup_irq(FALLING_EDGE);
        break;
        case FALLING_EDGE:
#if defined(ROTENC_INPUTS_INVERTED)
        rotenc_value -= ((read_chB() == 0) ? -1 : +1);   
#else
        rotenc_value += ((read_chB() == 0) ? -1 : +1);  // if B is low then CCW else CW
#endif /* ROTENC_INPUTS_INVERTED */
        rotenc_setup_irq(RISING_EDGE);
        break;
    }
}

void rotenc_set(rotenc_t value)
{
    rotenc_value = value;
}
    
void rotenc_clear()
{
    rotenc_set(0);
}

rotenc_t rotenc_accumulator()
{
    GIMSK &= ~(1 << ROTENC_CHA_INT); // disable interrupt
    rotenc_t tmp = rotenc_value;
    GIMSK |= (1 << ROTENC_CHA_INT); // enable interrupt
    return tmp;
}
